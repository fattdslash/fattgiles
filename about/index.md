---
layout: page
title: Acerca de mí
tags: [about, Jekyll, theme, moon]
date: 2022-02-03
comments: false
---

![](fattgiles2.png){: class="about-img-circle"}

# Fátima Rodríguez Giles

Ingeniera en Ciberseguridad en Ingeniería Dric, egresada de la carrera de Ingeniería en telecomunicaciones por la UNAM, Maestra en Ingeniería en Seguridad y Tecnologías de la Información por el Instituto Politécnico Nacional con experiencia en seguridad defensiva, ofensiva y regulación de la Seguridad de la Información y ciberseguridad. Profesional en Ciberseguridad con base en ISO27032:2012 y Hacker Ético por el ECCouncil.
